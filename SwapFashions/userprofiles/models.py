from django.db import models
from django.contrib.auth.models import User

ROLL_CHOICES = (
    ("superuser", "Superuser"),
    ("admin", "Admin"),
    ("user", "user"),
)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_profile')
    profile_pic = models.ImageField(upload_to='profiles/', blank=True, null=True)
    role = models.CharField(max_length=10,choices=ROLL_CHOICES,default="user")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)



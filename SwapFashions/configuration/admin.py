from django.contrib import admin

# Register your models here.

from configuration.models import ClothTypesMaster
from configuration.models import ClothCodesMaster
from configuration.models import ClothColourMaster
from configuration.models import ProductConditionMaster
from configuration.models import MaterialTypeMaster
from configuration.models import BustMaster
from configuration.models import WaistMaster
from configuration.models import HipMaster
from configuration.models import LengthMaster
from configuration.models import SizeMaster
from configuration.models import ProductCodeMaster


admin.site.register(ClothTypesMaster)
# admin.site.register(ClothCodesMaster)
admin.site.register(ClothColourMaster)
admin.site.register(ProductConditionMaster)
admin.site.register(MaterialTypeMaster)
admin.site.register(BustMaster)
admin.site.register(WaistMaster)
admin.site.register(HipMaster)
admin.site.register(LengthMaster)
admin.site.register(SizeMaster)
# admin.site.register(ProductCodeMaster)
"""
all configuration master tables data
"""
from django.db import models
from swap_fashion_apis.helpers.messages import API_RESPONSE_MSG
# Create your models here.


from swap_fashion_apis.models import AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel


class ClothTypesMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    cloth_type_name = models.CharField(max_length=150, unique=True)
    code = models.CharField(max_length=150, null=True, blank=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Cloth Types Master'
        verbose_name_plural = 'Cloth Types Master'
        db_table = 'cloth_type_master'

    def __str__(self):
        return str(self.cloth_type_name)
    
# not use
class ClothCodesMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    cloth_code = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Cloth Codes Master'
        verbose_name_plural = 'Cloth Codes Master'
        db_table = 'cloth_codes_master'

    def __str__(self):
        return str(self.cloth_code)


class ClothColourMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    colour_name = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Cloth Colour Master'
        verbose_name_plural = 'Cloth Colour Master'
        db_table = 'cloth_color_master'

    def __str__(self):
        return str(self.colour_name)


class ProductConditionMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    condition_name = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Product Condition Master'
        verbose_name_plural = 'Product Condition Master'
        db_table = 'product_condition_master'

    def __str__(self):
        return str(self.condition_name)


class MaterialTypeMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    material_type = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Material Type Master'
        verbose_name_plural = 'Material Type Master'
        db_table = 'material_type_master'

    def __str__(self):
        return str(self.material_type)


class BustMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    bust_number = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Bust Master'
        verbose_name_plural = 'Bust Master'
        db_table = 'bust_master'

    def __str__(self):
        return str(self.bust_number)


class WaistMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    waist_number = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Waist Master'
        verbose_name_plural = 'Waist Master'
        db_table = 'waist_master'

    def __str__(self):
        return str(self.waist_number)


class HipMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    hip_number = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Hip Master'
        verbose_name_plural = 'Hip Master'
        db_table = 'hip_master'

    def __str__(self):
        return str(self.hip_number)


class LengthMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    length = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Length Master'
        verbose_name_plural = 'Length Master'
        db_table = 'length_master'

    def __str__(self):
        return str(self.length)


class SizeMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    size = models.CharField(max_length=150, unique=True)
    code = models.CharField(max_length=150, null=True, blank=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Size Master'
        verbose_name_plural = 'Size Master'
        db_table = 'size_master'

    def __str__(self):
        return str(self.size)


# not use
class ProductCodeMaster(AbstractStatusModel, AbstractDateModel, AbstractCreateUpdateByModel):

    product_code = models.CharField(max_length=150, unique=True)
    description = models.CharField(max_length=255,
                                   null=True, blank=True,
                                   help_text=API_RESPONSE_MSG['HELP_DESC'])

    class Meta:
        verbose_name = 'Product Code Master'
        verbose_name_plural = 'Product Code Master'
        db_table = 'product_code_master'

    def __str__(self):
        return str(self.product_code)



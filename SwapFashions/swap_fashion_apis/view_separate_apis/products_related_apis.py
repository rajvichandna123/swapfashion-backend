
# Rest Apps
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework_tracking.mixins import LoggingMixin
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

# import helpers
from swap_fashion_apis.helpers.api_response_parser import APIResponseParser
from swap_fashion_apis.helpers.messages import API_RESPONSE_MSG
from swap_fashion_apis.helpers.utils import SerializerErrorParser

# import products
from apis.models import Product
from swap_fashion_apis.separate_serializers.product_serializers import ProductDetailSerializer 



class ProductViewDetails(LoggingMixin, APIView):
    """
    Args:
        LoggingMixin (mixinClass): track every api log
        APIView (superClass): genral api view
    path: /api/v1/product/details/17/
    autherization: True
    method:GET
    response:{
        "data": {
            "id": 17,
            "name": "top 8",
            "slug": "top-8",
            "product_option": "notVisible",
            "price": 1478,
            "discount": 14,
            "discount_visible": null,
            "is_available": true,
            "quantity": 1,
            "created": "2022-02-17T00:56:34.993353",
            "image": "https://swap-fashions-bucket.s3.amazonaws.com/media/images/top-8.jpg",
            "hover_image": "https://swap-fashions-bucket.s3.amazonaws.com/media/images/top-8_hjkbphu.jpg",
            "old_price": 1589,
            "old": "notVisible",
            "description": "mjuytghj",
            "emoji": "notVisible",
            "hot": "notVisible",
            "status": "new",
            "like": false,
            "Sub_Category": null,
            "Brands": null,
            "user": 1,
            "cloth_type": null,
            "code_size": null,
            "colour": null,
            "condition": null,
            "material_type": null,
            "bust": null,
            "waist": null,
            "hip": null,
            "length": null,
            "size": null,
            "code_name": null,
            "category": [
                2
            ]
        },
        "message": "DONE",
        "status": true,
        "errors": []
    }
    """
    # permission_classes = (IsAuthenticated, )
    # permission_classes = (permissions.AllowAny, )
    queryset = Product.objects.filter()
    serializer_class = ProductDetailSerializer
    
    def get(self, request, id):
        """"
        Args:
            request (django-request-instance): all request params like header, request_body
            product_id (int): product id use for one product detail
        """
        if not id:
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['PROVIDE_PRODUCT_ID'],
                status=False)
        try:
            return APIResponseParser.responses(
                status_code=status.HTTP_200_OK,
                message=API_RESPONSE_MSG['DONE'],
                data={'data': self.serializer_class(self.queryset.filter(id=id).last()).data}, 
                status=True,
            )
        except Exception as e:
            print(e)
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['EXCEPTION'],
                errors=[{'errors': str(e)}],
                status=False
            )
                

class AllFilterData(LoggingMixin, APIView):
    """
    Args:
        LoggingMixin (mixinClass): track every api log
        APIView (superClass): genral api view
    path: /api/v1/filter/data/
    autherization: True
    method:GET
    response:
    """
    
    def get(self, request):
        """"
        Args:
            request (django-request-instance): all request params like header, request_body
            product_id (int): product id use for one product detail
        """
        if not id:
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['PROVIDE_PRODUCT_ID'],
                status=False)
        try:
            return APIResponseParser.responses(
                status_code=status.HTTP_200_OK,
                message=API_RESPONSE_MSG['DONE'],
                data={'data': self.serializer_class(self.queryset.objects.filter(id=id).last()).data}, 
                status=True,
            )
        except Exception as e:
            print(e)
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['EXCEPTION'],
                errors=[{'errors': str(e)}],
                status=False
            )




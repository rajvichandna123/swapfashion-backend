from django.contrib.auth.models import User

# Rest Apps
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework_tracking.mixins import LoggingMixin
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

# import helpers
from swap_fashion_apis.helpers.api_response_parser import APIResponseParser
from swap_fashion_apis.helpers.messages import API_RESPONSE_MSG
from swap_fashion_apis.helpers.utils import SerializerErrorParser

# import user
from Users.models import TwoFactorAuthentication
from swap_fashion_apis.view_separate_apis.verification_process import \
     TWOFactorAuthenticationSystem
from swap_fashion_apis.helpers.all_configuration_data import get_auth_user_instance 
from swap_fashion_apis.separate_serializers.user_serializer import AuthUserSerializer



class AuthUserProfileAPI(LoggingMixin, APIView):
    """
    Args:
        LoggingMixin (mixinClass): track every api log
        APIView (superClass): genral api view
    path: /api/v1/user-details/
    autherization: True
    method:GET
    response: {
        "data": {
            "email": "admin@gmail.com",
            "id": 1
        },
        "message": "DONE",
        "status": true,
        "errors": []
    }
    """
    permission_classes = (IsAuthenticated, )
    # permission_classes = (permissions.AllowAny, )
    queryset = User.objects.filter()
    serializer_class = AuthUserSerializer
    
    def get(self, request):
        """"
        Args:
            request (django-request-instance): all request params like header, request_body
        """
        auth_user_instance = get_auth_user_instance(request.user.id)
        if not auth_user_instance:
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['USER_NOT_FOUND'],
                status=False)
        try:
            return APIResponseParser.responses(
                status_code=status.HTTP_200_OK,
                message=API_RESPONSE_MSG['DONE'],
                data={'data': self.serializer_class(auth_user_instance).data}, 
                status=True,
            )
        except Exception as e:
            print(e)
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['EXCEPTION'],
                errors=[{'errors': str(e)}],
                status=False
            )



class ResendOTPAPI(LoggingMixin, APIView):
    """
    Args:
        LoggingMixin (mixinClass): track every api log
        APIView (superClass): genral api view
    path: /api/v1/resend/
    autherization: False
    method:POST
    request: {
        "user_id": "10",
    }
    response: {
        "otp_resend": true,
        "message": "DONE",
        "status": true,
        "errors": []
    }
    """
    
    def post(self, request):
        """"
        Args:
            request (django-request-instance): all request params like header, request_body
        """
        if not request.data:
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['PROVIDE_REQUEST_DATA'],
                status=False)

        auth_user_instance = get_auth_user_instance(request.data['user_id'])
        if not auth_user_instance:
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['USER_NOT_FOUND'],
                status=False)

        otp_verification_instance = TWOFactorAuthenticationSystem(
            user=auth_user_instance, service='__VERIFICATION__')
        if not otp_verification_instance.generate_send_notification():
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message='resend_failed',
                status=False)

        return APIResponseParser.responses(
            status_code=status.HTTP_200_OK,
            message=API_RESPONSE_MSG['DONE'],
            data={'otp_resend': True}, 
            status=True)


class OTPVerificationAPI(LoggingMixin, APIView):
    """
    Args:
        LoggingMixin (mixinClass): track every api log
        APIView (superClass): genral api view
    path: /api/v1/otp/verification/
    autherization: False
    method:POST
    request: {
        "user_id": "10",
        "OTP": "393090"
    }
    response:{
        "otp_verification": true,
        "message": "DONE",
        "status": true,
        "errors": []
    }

    """
    
    def post(self, request):
        """"
        Args:
            request (django-request-instance): all request params like header, request_body
        """
        if not request.data:
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['PROVIDE_REQUEST_DATA'],
                status=False)

        auth_user_instance = get_auth_user_instance(request.data['user_id'])
        if not auth_user_instance:
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=API_RESPONSE_MSG['USER_NOT_FOUND'],
                status=False)

        otp_verification_instance = TWOFactorAuthenticationSystem(
            user=auth_user_instance, OTP=request.data.get('OTP'))
        otp_status, message = otp_verification_instance.otp_verification()
        if not otp_status:
            return APIResponseParser.responses(
                status_code=status.HTTP_400_BAD_REQUEST,
                message=message,
                status=False)

        return APIResponseParser.responses(
            status_code=status.HTTP_200_OK,
            message=API_RESPONSE_MSG['DONE'],
            data={'otp_verification': True}, 
            status=True)


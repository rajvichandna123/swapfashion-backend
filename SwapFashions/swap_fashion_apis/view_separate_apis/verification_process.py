"""UserOTPSystem
Created at 21 August 2021 by Gourav Sharma (^_^)
This user otp system used to generate the otp for a customer for verified account and 
also used with forgot verification
"""

import random

from datetime import datetime
from datetime import timedelta
from _thread import start_new_thread

from Users.models import TwoFactorAuthentication

# mailer module import
from swap_fashion_apis.helpers.mailer import Mailer
from swap_fashion_apis.helpers.messages import API_RESPONSE_MSG

def mail_sender(email_id, random_otp_number, service_name):
    """mail_sender
    this 'mail_sender' func used to sent the mail for a otp user
    """
    mailer_data = {
        'email_id': email_id,
        'random_otp_number': random_otp_number,
        'subject': 'otp generation',
        'mailer_template_name': 'mailer/verification.html',
        'service_name': service_name
    }
    _mailer_instance = Mailer(**mailer_data)
    mailer_status = _mailer_instance()
    print('## G30')
    print(mailer_status)


def get_the_otp():
    """get_the_otp
    this get_the_otp() methods used to generate the 6 digits random number
    for service provider or customer otp
    """
    return random.randint(100000, 999999)



class TWOFactorAuthenticationSystem:
    """
    TWOFASystem
    This TWOFASystem() class Module help the generate and sent the 6 digit otp for 
    customers user to authenticate valid users
    Methods:
        generate_otp_and_send_notification()
    """

    def __init__(self, **kwargs):
        self.user_instance = kwargs.get('user', None)
        self.service_name = kwargs.get('service', None)
        self.otp = kwargs.get('OTP')
        self.sms_otp_expiry = timedelta(minutes=30)
        self.otp_success_status = ''
        self.twoFA_type = 'link'
        self.activation_link_code = kwargs.get('activation_link', None)
        self.current_date_time = datetime.now()
        self.errors_result_dict = {
            "message": "user instance not valid for otp",
            "status": False
        }

    def __call__(self):
        if not self.user_instance:
            return self.errors_result_dict
        return self.generate_send_notification()

    def otp_verification(self):
        """
        This 'otp_verification' method used to verify the otp user
        """
        two_factor_instance = TwoFactorAuthentication.objects.filter(
            created_at__lte=self.current_date_time,
            expired_datetime__gte=self.current_date_time,
            user=self.user_instance).first()

        if not two_factor_instance:
            return False, API_RESPONSE_MSG['OTP_EXPIRED']

        if int(two_factor_instance.otp) == int(self.otp):
            try:
                two_factor_instance.is_verified = True
                two_factor_instance.save()
            except Exception as error:
                print(error)
            return True, API_RESPONSE_MSG['DONE']
        else:
            return False, API_RESPONSE_MSG['OTP_MISMATCH']


    def generate_send_notification(self):
        """generate_send_notification
        This generate_send_notification() method used to sent the notification for 
        user and generate the otp with link by user db
        In this method we used to customize the self otp with entry in db 
        models to track  how many sent notification
        and verified users
        """
        random_otp_number = get_the_otp()
        try:
            start_new_thread(
                mail_sender, (
                    self.user_instance.email, 
                    random_otp_number,
                    self.service_name
                )
            )
            self.otp_success_status = 'successful'
        except Exception as error:
            print(error)
            self.otp_success_status = 'not_delivered'

        factor_generation_data = {
            'twoFA_type': self.twoFA_type,
            'activation_link': None,
            'user': self.user_instance,
            'otp': random_otp_number,
            'delivery_status': self.otp_success_status,
            'user_type': 'customer',
            'auth_type': 'email',
            'expired_datetime': datetime.now() + self.sms_otp_expiry
        }
        otp_instance = TwoFactorAuthentication.objects.create(**factor_generation_data)
        return True if otp_instance else False







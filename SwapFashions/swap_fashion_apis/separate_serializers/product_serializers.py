from rest_framework import serializers
from apis.models import Product, Category

from Coins.models import CoinManagement



class CategorySerializer(serializers.ModelSerializer):
    """
    serializer for categories that serialize all of the fields
    based on Category model

    """

    class Meta:
        model = Category
        fields = '__all__'

class ProductDetailSerializer(serializers.ModelSerializer):

    category = CategorySerializer(many=True)

    cloth_type = serializers.SerializerMethodField()
    code_size = serializers.SerializerMethodField()
    colour = serializers.SerializerMethodField()
    condition = serializers.SerializerMethodField()
    material_type = serializers.SerializerMethodField()
    bust = serializers.SerializerMethodField()
    waist = serializers.SerializerMethodField()
    hip = serializers.SerializerMethodField()
    length = serializers.SerializerMethodField()
    size = serializers.SerializerMethodField()
    code_name = serializers.SerializerMethodField()
    sku = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()

    # all new fields
    Brands = serializers.SerializerMethodField()
    sale_coins = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()



    class Meta:
        model = Product
        fields = '__all__'

    def get_Brands(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(instance.Brands.name)
        except Exception as e:
            print(e)
            return None

    def get_sale_coins(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(CoinManagement.objects.filter(product=instance).last().sale_coins)
        except Exception as e:
            print(e)
            return None

    def get_price(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(CoinManagement.objects.filter(product=instance).last().sale_price)
        except Exception as e:
            print(e)
            return None

    def get_sku(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(instance.sku)
        except Exception as e:
            print(e)
            return None

    def get_title(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(instance.title)
        except Exception as e:
            print(e)
            return None
            
    def get_cloth_type(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(instance.cloth_type.cloth_type_name)
        except Exception as e:
            print(e)
            return None

    def get_code_size(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.code_size.cloth_code
        except Exception as e:
            print(e)
            return None

    def get_colour(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.colour.colour_name
        except Exception as e:
            print(e)
            return None


    def get_condition(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.condition.condition_name
        except Exception as e:
            print(e)
            return None

    def get_material_type(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.material_type.material_type
        except Exception as e:
            print(e)
            return None


    def get_bust(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.bust.bust_number
        except Exception as e:
            print(e)
            return None


    def get_waist(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.waist.waist_number
        except Exception as e:
            print(e)
            return None


    def get_hip(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.hip.hip_number
        except Exception as e:
            print(e)
            return None


    def get_length(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.length.length
        except Exception as e:
            print(e)
            return None


    def get_size(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.size.size
        except Exception as e:
            print(e)
            return None


    def get_code_name(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.code_name.product_code
        except Exception as e:
            print(e)
            return None

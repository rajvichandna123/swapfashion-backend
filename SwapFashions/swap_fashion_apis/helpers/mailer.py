"""Mailer Sender System
created at 26 september 2021 by GouravSharma(^_^)
This module used to sent and manage the mail system using django-mailer system
"""
from datetime import datetime

from django.core.mail import EmailMultiAlternatives
from django.core.mail import send_mail
from django.utils.html import strip_tags

from django.template.loader import render_to_string

from SwapFashions.settings import EMAIL_HOST_USER

from swap_fashion_apis.helpers.mailer_message import get_template_message


class Mailer:
    """
    Mailer
    used for sending the mail
    """

    def __init__(self, **kwargs):

        # emailer data
        self.template_data = kwargs
        self.random_otp_number = kwargs.get('random_otp_number', None)
        self.default_template = 'mailer/verification.html'
        self.mailer_template = kwargs.get('mailer_template_name', self.default_template)
        self.email_subject = kwargs.get('subject')
        self.email_status = False
        self.notification_category = "EMAIL"
        self.service_name = kwargs.get('service_name', '__VERIFICATION__')
        self.to_email_id = kwargs.get('email_id')
        self.cc = 'pycodertest@gmail.com'
        # failed reason error
        self.reason_for_failed = 'Error'

    def __call__(self): return self.email_sender()

    def email_sender(self):
        """email_sender
        This "email_sender" used to send the mail for each user as per logic to send the mail 
        for all type users
        return: status=True/False
            # template message add from templates message module according
            # to service and user logic
        """
        self.template_data['code'] = self.random_otp_number
        self.template_data['message'] = get_template_message(self.service_name, self.template_data)
        try:
            html_render_content = render_to_string(
                self.mailer_template,
                self.template_data)
            msg = EmailMultiAlternatives(
                self.email_subject,
                strip_tags(html_render_content),
                EMAIL_HOST_USER,
                [self.to_email_id, self.cc],
                cc=[]
            )
            msg.attach_alternative(html_render_content, "text/html")
            return True if msg.send() else False
        except Exception as exception_error:
            print("## G76")
            print(exception_error)
            self.reason_for_failed = str(exception_error)
            return False

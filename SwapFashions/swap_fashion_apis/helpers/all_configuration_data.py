from django.contrib.auth.models import User



def get_auth_user_instance(user_id):
	"""
	get auth user instance using user isd
	"""
	try:
		return User.objects.filter(id=user_id).last()
	except Exception as e:
		print(e)
		return None
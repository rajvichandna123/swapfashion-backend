API_RESPONSE_MSG = dict(
    DONE="DONE",
    PROVIDE_PRODUCT_ID="Please Provide product id",
    HELP_DESC='For Backend Purpose',
    PROVIDE_REQUEST_DATA="Request not defined please post the json request",
    USER_NOT_FOUND="User Not Found please post the valid user id",
    OTP_EXPIRED="Your OTP Expired Please resend the otp",
    OTP_MISMATCH="Your otp is not correct please enter correct OTP",
    EXCEPTION="exception"    
)
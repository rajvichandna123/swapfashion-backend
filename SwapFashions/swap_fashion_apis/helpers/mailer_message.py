"""
mailer templates message
"""



TEMPLATE_MESSAGE = dict(
    __VERIFICATION__='Your verification code is : [CODE]',
)


def get_template_message(service_name, template_data):
    """get_template_message
    This 'get_template_message' function used to get the template message according to
    service logic to change with add template data
    params: service_name, template_data
    return: string_message
    """
    if service_name == '__VERIFICATION__':
        return TEMPLATE_MESSAGE.get(service_name) \
            .replace('[CODE]', str(template_data.get('random_otp_number')))

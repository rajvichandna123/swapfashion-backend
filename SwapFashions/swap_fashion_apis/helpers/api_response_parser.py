"""
API RESPONSE PARSER
"""

from rest_framework import status
from rest_framework.response import Response


class APIResponseParser:
    """
    ApiResponseParser
    Common api Response Parser for all API Response.
    """

    def __init__(self):
        pass

    @staticmethod
    def responses(**response_data):
        """
        responses: response_with_status
        This response_with_status() methods used to get and create the response with status
        code api structure data
        kwargs: all Kwargs Parameter comes from to APIView Class.
        return: Response
        """
        response = {}
        try:
            if response_data.get('status', True):
                for key, values in response_data['data'].items():
                    response[key] = values
                response['message'] = response_data.get('message', 'DONE')
                response['status'] = response_data.get('status', True)
                response['errors'] = response_data.get('errors', [])
                return Response(response, status=response_data.get('status_code'))
            return Response(
                {'message': response_data.get('message', 'ERRORs'), 'status': False, 
                 'errors': response_data.get('errors', []), 'data': response_data.get('data', {})}, 
                 status=response_data.get('status_code', status.HTTP_302_FOUND))
        except Exception as msg:
            return Response(
                {'message': "APIResponseParser.response.errors", 'errors': [str(msg)], 'status': False},
                status=response_data.get('status_code', status.HTTP_302_FOUND))

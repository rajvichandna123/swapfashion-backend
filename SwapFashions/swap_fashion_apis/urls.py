from django.urls import path
from swap_fashion_apis.view_separate_apis.products_related_apis import ProductViewDetails
from swap_fashion_apis.view_separate_apis.otp_verification import OTPVerificationAPI
from swap_fashion_apis.view_separate_apis.otp_verification import ResendOTPAPI
from swap_fashion_apis.view_separate_apis.otp_verification import AuthUserProfileAPI

urlpatterns = [
    path('product/detail/<int:id>/', ProductViewDetails.as_view()),
    path('otp/verification/', OTPVerificationAPI.as_view()),
    path('resend/', ResendOTPAPI.as_view()),
    path('user-details/', AuthUserProfileAPI.as_view()),

]

from django.apps import AppConfig


class SwapFashionApisConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'swap_fashion_apis'

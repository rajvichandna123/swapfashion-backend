from rest_framework import serializers
from .models import (
    Brands,
    Gender,
    Product,
    Category,
    CartItem,
    Sub_Category,
    Orders
)
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from Coins.models import CoinManagement


class CategorySerializer(serializers.ModelSerializer):
    """
    serializer for categories that serialize all of the fields
    based on Category model

    """

    class Meta:
        model = Category
        fields = '__all__'


class Sub_CategorySerializer(serializers.ModelSerializer):
    """
    serializer for Sub_categories that serialize all of the fields
    based on Category model

    """

    class Meta:
        model = Sub_Category
        fields = '__all__'


class BrandsSerializer(serializers.ModelSerializer):
    """
    serializer for Sub_categories that serialize all of the fields
    based on Category model

    """

    class Meta:
        model = Brands
        fields = '__all__'


class GenderSerializer(serializers.ModelSerializer):
    """
    serializer for Gender that serialize all of the fields
    based on Category model

    """

    class Meta:
        model = Gender
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    """
    serializer for products that serialize:
    (id', 'url', "name", "slug", "category", "price", "discount", "available",
    "quantity", "created", "image", "description")
    and add relation to category serializer \nbased on Product model

    """

    category = CategorySerializer(many=True)

    cloth_type = serializers.SerializerMethodField()
    code_size = serializers.SerializerMethodField()
    colour = serializers.SerializerMethodField()
    condition = serializers.SerializerMethodField()
    material_type = serializers.SerializerMethodField()
    bust = serializers.SerializerMethodField()
    waist = serializers.SerializerMethodField()
    hip = serializers.SerializerMethodField()
    length = serializers.SerializerMethodField()
    size = serializers.SerializerMethodField()
    code_name = serializers.SerializerMethodField()

    # all new fields
    Brands = serializers.SerializerMethodField()
    sale_coins = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = (

            'id', 'sku', 'title', 'url', "name", "slug", "category", "price", "discount",
            "available", "quantity", "created", "image", "hover_image", "old",
            "discount_visible", "old_price",
            "emoji", "hot", "status", "like", "description",

            "cloth_type",
            "code_size",
            "colour",
            "condition",
            "material_type",
            "bust",
            "waist",
            "hip",
            "length",
            "size",
            "code_name",

            'Brands',
            'sale_coins'
        )

    def get_Brands(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(instance.Brands.name)
        except Exception as e:
            print(e)
            return None

    def get_sale_coins(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(CoinManagement.objects.filter(product=instance).last().sale_coins)
        except Exception as e:
            print(e)
            return None

    def get_price(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(CoinManagement.objects.filter(product=instance).last().sale_price)
        except Exception as e:
            print(e)
            return None

    def get_cloth_type(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return str(instance.cloth_type.cloth_type_name)
        except Exception as e:
            print(e)
            return None

    def get_code_size(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.code_size.cloth_code
        except Exception as e:
            print(e)
            return None

    def get_colour(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.colour.colour_name
        except Exception as e:
            print(e)
            return None

    def get_condition(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.condition.condition_name
        except Exception as e:
            print(e)
            return None

    def get_material_type(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.material_type.material_type
        except Exception as e:
            print(e)
            return None

    def get_bust(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.bust.bust_number
        except Exception as e:
            print(e)
            return None

    def get_waist(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.waist.waist_number
        except Exception as e:
            print(e)
            return None

    def get_hip(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.hip.hip_number
        except Exception as e:
            print(e)
            return None

    def get_length(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.length.length
        except Exception as e:
            print(e)
            return None

    def get_size(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.size.size
        except Exception as e:
            print(e)
            return None

    def get_code_name(self, instance):
        """
        this function used to add full name of candidate(service provider) details
        """
        try:
            return instance.code_name.product_code
        except Exception as e:
            print(e)
            return None


# class ProductSerializer(serializers.HyperlinkedModelSerializer):
#     """
#     serializer for products that serialize:
#     (id', 'url', "name", "slug", "category", "price", "discount", "available",
#     "quantity", "created", "image", "description")
#     and add relation to category serializer \nbased on Product model

#     """

#     category = CategorySerializer(many=True)

#     class Meta:
#         model = Product
#         fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    """
    serializer for user that serialize :
    ('id', 'username', 'first_name', 'last_name', 'email', 'image',
    'is_staff', 'is_active', 'is_superuser')\nbased on default 'User' model

    """

    class Meta:
        model = User
        fields = (
            'id', 'username', 'first_name', 'last_name', 'email', 'image',
            'is_staff', 'is_active', 'is_superuser')

    image = serializers.ImageField(source='profile.image', required=False)


class CartItemSerializer(serializers.ModelSerializer):
    """
    serializer for cartitem that serialize all fields in 'CartItem' class
    model and add 'product' as relation

    """

    product = ProductSerializer()
    cart_sub_total = serializers.SerializerMethodField()

    class Meta:
        model = CartItem
        fields = ('id', 'quantity', 'product', 'cart_sub_total')

    def get_cart_sub_total(self, instance):
        """
        get_cart_sub_total
        """
        try:
            sub_total = 0
            if instance.user:
                for item in CartItem.objects.filter(user=instance.user):
                    sub_total += CoinManagement.objects.filter(
                        product=item.product
                    ).last().sale_price * instance.quantity
            return sub_total
        except Exception as e:
            print(e)
            return None


class CartItemAddSerializer(serializers.ModelSerializer):
    product_id = serializers.IntegerField()

    class Meta:
        model = CartItem
        fields = ('product_id',)
        extra_kwargs = {
            # 'quantity': {'required': True},
            'product_id': {'required': True},
        }

    def create(self, validated_data):
        try:
            user = User.objects.get(id=self.context['request'].user.id)
            product = get_object_or_404(
                Product, id=validated_data['product_id'])
            try:
                if product.quantity == 0 or product.is_available is False:
                    raise serializers.ValidationsError(
                        {'not available': 'the product is not available.'})
            except Exception as e:
                print('G291')
                print(e)
            try:
                if CartItem.objects.filter(product=product, user=user).exists():
                    raise serializers.ValidationsError(
                        'This product is already in your cart.')
            except Exception as e:
                print('exception-error')
                print(e)
            cart_item = CartItem.objects.create(
                product=product,
                user=user,
            )
            cart_item.save()
            cart_item.add_amount()
            # product.quantity = product.quantity - cart_item.quantity
            product.save()
            return cart_item
        except Exception as e:
            print('G303')
            print(e)
            return None


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orders
        fields = [
            'order_id',
            'First_name',
            'last_name',
            'email',
            'address',
            'amount',
            'city',
            'state',
            'zip_code',
            'phone',
            'cart',
            'total_price',
            'payment_mode',
            'payment_key'
        ]


import json
import requests
import base64
import json
from SwapFashions_settings import SHIP_ROCKET_BASE_URL
from card.models import Cart
from order.models import ShipmentOrder, Order

from helper.modules.mailer import Mailer
from _thread import start_new_thread

SHIP_ROCKET_BASE_URL = 'https://apiv2.shiprocket.in/v1/external/'

LOGIN_URL = SHIP_ROCKET_BASE_URL+'auth/login'
CREAT_ORDER_URL = SHIP_ROCKET_BASE_URL+'orders/create/adhoc'
CANCEL_ORDER_URL = SHIP_ROCKET_BASE_URL+'orders/cancel'
AWB_CODE_URL = SHIP_ROCKET_BASE_URL+'courier/assign/awb'
SHIPMENT_URL = SHIP_ROCKET_BASE_URL+'courier/generate/pickup'
INVOICE_URL = SHIP_ROCKET_BASE_URL+'orders/print/invoice'
TRACKING_URL = SHIP_ROCKET_BASE_URL+'courier/track/shipment/'
ADD_PRODUCT_URL = SHIP_ROCKET_BASE_URL+'products'


def mailer(email_id, order_id, subject, mailer_template_name, service_name):
    """
    mailer
    """
    mailer_obj = Mailer(email_id=email_id,
                        order_id=order_id,
                        subject=subject,
                        mailer_template_name=mailer_template_name,
                        service_name=service_name)
    # mailer instance object
    mailer_status = mailer_obj()
    print(mailer_status)


class ShipRocketAPIServices:

    def __init__(self, *args, **kwargs):
        self.api_name = kwargs.get('api_name', None)
        self.cart_id = kwargs.get('cart_id', None)
        self.order = kwargs.get('order', None)
        self.name = kwargs.get('name', None)
        # self.type = kwargs['type']
        self.quantity = kwargs.get('quantity', None)
        self.sku = kwargs.get('sku', None)
        self.inventory_id = kwargs.get('inventory_id', None)
        self.inventory_qty = kwargs.get('inventory_qty', None)
        self.shipment_id = kwargs.get('shipment_id', None)

    def __call__(self):

        if self.api_name == 'order-tracking':
            return self.order_tracking()
        if self.api_name == 'create-order':
            return self.create_or_update_order()
        if self.api_name == 'generate-invoice':
            return self.generate_invoice()
        if self.api_name == 'request-shipment':
            return self.request_for_shipment()
        if self.api_name == 'awb-generate':
            return self.generate_awb_code()
        if self.api_name == 'cancel-order':
            return self.cancel_an_order()
        if self.api_name == 'manual_cancel_order':
            return self.manual_cancel_order()
        if self.api_name == 'add-product':
            return self.add_product()
        if self.api_name == 'inventory-data':
            return self.get_inventory_data()
        if self.api_name == 'update-inventory':
            return self.update_inventory()
        if self.api_name == 'get_detail':
            return self.get_detail()

    def create_token(self):
        # request_obj = {
        #     "email": "gaurav.sharma@loopmethods.com",
        #     "password": "admin@123"
        # }
        # response_obj = requests.post(LOGIN_URL, data=json.dumps(request_obj), headers={'Content-Type': 'application/json'})
        # if response_obj.status_code == 200:
        #     return response_obj.json()
        # return {}
        url = "https://apiv2.shiprocket.in/v1/external/auth/login"

        payload = json.dumps({
            "email": "sahilrana825@gmail.com",
            "password": "s9a8h0i5l8"
        })
        headers = {
            'Content-Type': 'application/json'
        }

        response_obj = requests.request(
            "POST", url, headers=headers, data=payload)
        if response_obj.status_code == 200:
            return response_obj.json()['token']
        return {}

    def create_or_update_order(self):
        cart_instance = Cart.objects.get(id=self.cart_id)
        address = cart_instance.cart_address
        phone_number = str(address.phone_number)[3:]
        order_instance = Order.objects.get(id=self.order.id)
        order_items = []
        for cart_product in cart_instance.products.all():
            items = cart_product.product
            items = {
                "name": cart_product.product.title,
                "sku": cart_product.product.sku,
                "units": cart_product.quantity,
                "selling_price": float(cart_product.amount),
                "discount": "",
                "tax": "",
                "hsn": ''
            }
            order_items.append(items)
        url = "https://apiv2.shiprocket.in/v1/external/orders/create/adhoc"
        _phone_number = str(address.phone_number)[3:]
        payload = json.dumps({
            "order_id": order_instance.order_id,
            "order_date": "2021-09-02 11:11",
            "pickup_location": "Primary",
            "channel_id": "",
            "comment": "",
            "billing_customer_name": address.first_name,
            "billing_last_name": address.last_name,
            "billing_address": address.line1,
            "billing_address_2": address.line2,
            "billing_city": str(address.city),
            "billing_pincode": address.postcode,
            "billing_state": str(address.state),
            "billing_country": str(address.country),
            "billing_email": address.email,
            "billing_phone": _phone_number,
            "shipping_is_billing": True,
            "shipping_customer_name": "",
            "shipping_last_name": "",
            "shipping_address": "",
            "shipping_address_2": "",
            "shipping_city": "",
            "shipping_pincode": "",
            "shipping_country": "",
            "shipping_state": "",
            "shipping_email": "",
            "shipping_phone": "",
            "order_items": order_items,
            "payment_method": "COD",
            "shipping_charges": 0,
            "giftwrap_charges": 0,
            "transaction_charges": 0,
            "total_discount": float(cart_instance.code_discount),
            "sub_total": float(cart_instance.payable_amount),
            "length": 10,
            "breadth": 15,
            "height": 20,
            "weight": 2.5
        })
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }
        response_obj = requests.request(
            "POST", url, headers=headers, data=payload)
        if response_obj.status_code == 200:
            order_instance.status = 'Shipped'
            order_instance.save()
            _data = response_obj.json()
            shipment_instance = ShipmentOrder.objects.create(
                order=order_instance,
                order_number=_data['order_id'],
                shipment_id=_data['shipment_id'],
                status=_data['status'],
                onboarding_completed_now=_data['onboarding_completed_now'],
                awb_code=_data['awb_code'],
                courier_company_id=['courier_company_id'],
                courier_name=['courier_name']
            )
            shipment_instance = ShipmentOrder.objects.get(
                id=shipment_instance.id)
            order_id = shipment_instance.order_number
            url = "https://apiv2.shiprocket.in/v1/external/orders/print/invoice"
            payload = json.dumps({
                "ids": [order_id]
            })
            response = requests.request(
                "POST", url, headers=headers, data=payload)
            if response.status_code == 200:
                _data = response.json()
                shipment_instance.invoice_url = _data['invoice_url']
                shipment_instance.save()
                return response.json()
            return response_obj
        return response_obj

    def cancel_an_order(self):
        """
        cancel_an_order
        """
        order_instance = Order.objects.get(order_id=self.order)
        shipment_instance = order_instance.shipment_order
        order_id = shipment_instance.order_number
        print("order id")
        print(order_id)
        request_obj = {
            "ids": [order_id, ]
        }
        url = "https://apiv2.shiprocket.in/v1/external/orders/cancel"

        payload = json.dumps({
            "ids": [
                order_id,
            ]
        })
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }
        response_obj = requests.request(
            "POST", url, headers=headers, data=payload)
        if response_obj.status_code == 200:
            shipment_instance.status = 'Cancelled'
            shipment_instance.save()
            # for customer not cancel because of shiprocket wallet empty
            try:
                order_instance.status = 'Cancelled by Shiprocket'
                order_instance.save()
            except Exception as e:
                print('G200')
                print(e)

            try:
                start_new_thread(mailer, ('care@petrimedica.com',
                                          str(order_instance.order_id),
                                          'Ship Rocket Wallet Issue',
                                          'mailer/ship_rocket_wallet.html',
                                          '__SHIP_ROCKET_WALLET__'))
            except Exception as e:
                print('G194')
                print(e)
        else:
            return {}

    def manual_cancel_order(self):
        """
        manual_cancel_order
        created and updated by Gourav Sharma (^_^) reference with ajamal 
        cancel logic code for manual cancel evenets
        """
        order_instance = Order.objects.get(order_id=self.order)
        shipment_instance = order_instance.shipment_order
        order_id = shipment_instance.order_number
        request_obj = {"ids": [order_id, ]}
        payload = json.dumps({
            "ids": [
                order_id,
            ]
        })
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }
        response_obj = requests.request(
            "POST", "https://apiv2.shiprocket.in/v1/external/orders/cancel",
            headers=headers, data=payload)
        if response_obj.status_code == 200:
            shipment_instance.status = 'Cancelled'
            shipment_instance.save()
            # for customer not cancel because of shiprocket wallet empty
            try:
                order_instance.status = 'Cancelled'
                order_instance.save()
            except Exception as e:
                print('G200')
                print(e)

        #     try:
        #         start_new_thread(mailer, ('care@petrimedica.com',
        #                                   str(order_instance.order_id),
        #                                   'Ship Rocket Wallet Issue',
        #                                   'mailer/ship_rocket_wallet.html',
        #                                   '__SHIP_ROCKET_WALLET__'))
        #     except Exception as e:
        #         print('G194')
        #         print(e)
        # else:
        #     return {}

    def generate_awb_code(self):
        shipment_instance = ShipmentOrder.objects.get(order=self.order)
        shipment_id = shipment_instance.shipment_id
        url = "https://apiv2.shiprocket.in/v1/external/courier/assign/awb"
        payload = json.dumps({
            "shipment_id": shipment_id,
            "courier_id": ""
        })
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        if response.status_code == 200:
            if 'response' in list(response.json()):
                _data = response.json()
                shipment_instance.awb_code = _data['response']['data']['awb_code']
                shipment_instance.save()
                return response.json()
        return response

    def request_for_shipment(self):
        shipment_instance = ShipmentOrder.objects.get(order=self.order)
        shipment_id = shipment_instance.shipment_id
        url = "https://apiv2.shiprocket.in/v1/external/courier/generate/pickup"
        payload = json.dumps({
            "shipment_id": [
                shipment_id
            ]
        })
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        if response.status_code == 200:
            _data = response.json()
            shipment_instance.pickup_date = _data['response']['pickup_scheduled_date']
            shipment_instance.pickup_token_number = _data['response']['pickup_token_number']
            shipment_instance.pickup_data = _data['response']['data']
            shipment_instance.save()
            return response.json()
        return response

    def generate_invoice(self):
        order_instance = self.order
        shipment_instance = ShipmentOrder.objects.get(order=order_instance)
        order_id = shipment_instance.order_number
        url = "https://apiv2.shiprocket.in/v1/external/orders/print/invoice"
        payload = json.dumps({
            "ids": [order_id]
        })
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        if response.status_code == 200:
            _data = response.json()
            shipment_instance.invoice_url = _data['invoice_url']
            shipment_instance.save()
            return response.json()
        return {}

    def order_tracking(self):
        order_instance = Order.objects.get(id=self.order.id)
        shipment_instance = ShipmentOrder.objects.get(order=order_instance)
        shipment_id = shipment_instance.shipment_id
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }
        url = "https://apiv2.shiprocket.in/v1/external/courier/track/shipment/"+shipment_id
        payload = {}
        response = requests.request("GET", url, headers=headers, data=payload)
        print("tracking")
        print(response.json())
        if response.status_code == 200:
            _data = response.json()
            print("status")
            print(_data['tracking_data']['track_status'])
            if _data['tracking_data']['track_status'] == 1:
                print("url")
                print(_data['tracking_data']['track_url'])
                shipment_instance.tracking_url = _data['tracking_data']['track_url']
                shipment_instance.save()
                return response.json()
        return {}

    def add_product(self):
        url = "https://apis.shiprocket.in/apis/product"

        payload = json.dumps({
            "name": self.name,
            "category_code": 'default',
            "type": "Single",
            "qty": self.quantity,
            "sku": self.sku
        })
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }

        _response = requests.request(
            "POST", url, headers=headers, data=payload)
        if _response.status_code == 200:
            return _response.text
        else:
            return {}

    def get_inventory_data(self):

        url = "https://apiv2.shiprocket.in/v1/external/inventory"

        payload = {}
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        if response.status_code == 200:
            return response.json()
        else:
            return {}

    def update_inventory(self):

        url = "https://apiv2.shiprocket.in/v1/external/inventory/" + \
            str(self.inventory_id)+"/update"
        payload = json.dumps({
            "quantity": self.inventory_qty,
            "action": "remove"
        })
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }

        response = requests.request("PUT", url, headers=headers, data=payload)
        if response.status_code == 200:
            return response.json()
        else:
            return {}

    def get_detail(self):

        # order_instance = Order.objects.get(id=self.order.id)
        # shipment_instance = ShipmentOrder.objects.get(order=order_instance)
        # shipment_id = shipment_instance.shipment_id
        url = "https://apiv2.shiprocket.in/v1/external/shipments/" + \
            str(self.shipment_id)

        payload = {}
        _token = self.create_token()
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _token
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        if response.status_code == 200:
            return response.json()['data']
        else:
            return {}

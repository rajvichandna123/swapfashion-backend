from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'apis'
    verbose_name = 'catalogue'
    verbose_name_plural = 'catalogue'

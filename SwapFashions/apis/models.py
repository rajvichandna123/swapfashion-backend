from turtle import mode
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_delete
from django.dispatch.dispatcher import receiver
from datetime import datetime

from django.forms import BooleanField

from configuration.models import ClothTypesMaster
from configuration.models import ClothCodesMaster
from configuration.models import ClothColourMaster
from configuration.models import ProductConditionMaster
from configuration.models import MaterialTypeMaster
from configuration.models import BustMaster
from configuration.models import WaistMaster
from configuration.models import HipMaster
from configuration.models import LengthMaster
from configuration.models import SizeMaster
from configuration.models import ProductCodeMaster

# import coin master management table
from Coins.models import CoinManagement


def product_image(instance, filename):
    return 'images/{0}.jpg'.format(instance.slug)
    return 'images/{1}.jpg'.format(instance.slug)


def user_images(instance, filename):
    date_time = datetime.now().strftime("%Y_%m_%d,%H:%M:%S")
    saved_file_name = instance.user.username + "-" + date_time + ".jpg"
    return 'profile/{0}/{1}'.format(instance.user.username, saved_file_name)


def get_superuser():
    user = User.objects.filter(is_superuser=True).first()
    return user


class Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    is_lux = models.BooleanField(default=False)

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

class Sub_Category(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    is_lux = models.BooleanField(default=False)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Sub category'
        verbose_name_plural = 'Sub_categories'

    def __str__(self):
        return self.name

class Brands(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    is_lux = models.BooleanField(default=False)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Brands'
        verbose_name_plural = 'Brands'

    def __str__(self):
        return str(self.name)

class Gender(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200)
    is_lux = models.BooleanField(default=False)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Gender'
        verbose_name_plural = 'Gender'


    def __str__(self):
        return self.name

class AvailableManager(models.Manager):
    def get_queryset(self):
        return super(AvailableManager, self).get_queryset().filter(
            is_available=True,
            quantity__gte=1)


class Product(models.Model):

    name = models.CharField(max_length=150, unique=True, null=True, blank=True)
    description = models.TextField()

    slug = models.SlugField(unique=True, null=True, blank=True)
    category = models.ManyToManyField(Category, related_name='products')

    Sub_Category = models.ForeignKey(Sub_Category,on_delete=models.CASCADE,null=True, blank=True)
    Brands = models.ForeignKey(Brands,on_delete=models.CASCADE,null=True, blank=True)
    
    product_option = models.CharField(max_length=200, unique=False, null=True,blank=True)
    
    is_available = models.BooleanField(default=True)
    quantity = models.PositiveIntegerField()
    objects = models.Manager()
    available = AvailableManager()
    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.SET(get_superuser)) 
    
    emoji = models.CharField(max_length=200, unique=False, null=True,blank=True)
    hot = models.CharField(max_length=200, unique=False, null=True,blank=True)
    status = models.CharField(max_length=200, unique=False, null=True,blank=True)
    like = models.BooleanField(default=False)

    price = models.PositiveIntegerField(verbose_name='MRP')

    discount = models.PositiveIntegerField(default=0)
    discount_visible = models.CharField(max_length=200, unique=False, null=True, blank=True)
    old_price = models.PositiveIntegerField(null=True, blank=True)
    old = models.CharField(max_length=200, unique=False, null=True, blank=True)

    image = models.ImageField(upload_to=product_image, verbose_name='Image 1')
    hover_image = models.ImageField(upload_to=product_image, verbose_name='Image 2/ hover image')

    image_3 = models.ImageField(upload_to=product_image, verbose_name='Image 3', null=True,blank=True)
    image_4 = models.ImageField(upload_to=product_image, verbose_name='Image 4', null=True,blank=True)
    image_5 = models.ImageField(upload_to=product_image, verbose_name='Image 5', null=True,blank=True)
    image_6 = models.ImageField(upload_to=product_image, verbose_name='Image 6', null=True,blank=True)
    # all filter category

    cloth_type = models.ForeignKey(
        ClothTypesMaster, 
        on_delete=models.CASCADE, 
        null=True, blank=True, 
        related_name="cloth_type_prduct")
    code_size = models.ForeignKey(
        ClothCodesMaster, 
        on_delete=models.CASCADE,
        null=True, blank=True,
        related_name="cloth_code_prduct", verbose_name='Size Code')
    colour = models.ForeignKey(
        ClothColourMaster, 
        on_delete=models.CASCADE, 
        null=True, blank=True,
        related_name="cloth_color_prduct")
    condition = models.ForeignKey(
        ProductConditionMaster,
        on_delete=models.CASCADE, 
        null=True, blank=True,
        related_name="condtion_prduct")
    material_type = models.ForeignKey(
        MaterialTypeMaster, 
        on_delete=models.CASCADE,
        null=True, blank=True, 
        related_name="material_prduct")
    bust = models.ForeignKey(
        BustMaster,
        on_delete=models.CASCADE, 
        null=True, blank=True,
        related_name="bust_prduct")
    waist = models.ForeignKey(
        WaistMaster,
        on_delete=models.CASCADE,
        null=True, blank=True,
        related_name="waiset_prduct")
    hip = models.ForeignKey(
        HipMaster,
        on_delete=models.CASCADE, 
        null=True, blank=True,
        related_name="hip_prduct")
    length = models.ForeignKey(
        LengthMaster,
        on_delete=models.CASCADE,
        null=True, blank=True,
        related_name="lenth_prduct")
    size = models.ForeignKey(
        SizeMaster, on_delete=models.CASCADE,
        null=True, blank=True, related_name="size_prduct")
    code_name = models.ForeignKey(
        ProductCodeMaster, on_delete=models.CASCADE, 
        null=True, blank=True, related_name="product_prduct")

    def __str__(self):
        try:
            return str(self.Brands.name)
        except Exception as e:
            print(e)
            return '-'

    @property
    def sku(self):
        """
        :return: sku
        """
        try:
            if self.pk and self.cloth_type and self.created and self.code_size:
                return ''.join([
                    'SF', 
                    self.created.strftime('%y'), 
                    self.created.strftime('%m'), 
                    self.cloth_type.code, self.size.code, '00' ,str(self.pk)])
            return ''.join(['SF', '00', str(self.pk)])
        except Exception as e:
            print(e)
            return '-'

    @property
    def title(self):
        """
        :return: sku
        """
        try:
            if self.Brands and self.colour and self.cloth_type:
                return ''.join([self.Brands.name, ' ', self.colour.colour_name, 
                                ' ', self.cloth_type.cloth_type_name ])
            else:
                return ''.join([self.name, ' ', self.colour.colour_name, ' ', 
                                self.cloth_type.cloth_type_name ])
        except Exception as e:
            print(e)
            return '-'


    def save(self, *args, **kwargs):
        """save function used to automation the coin configuration tables """
        try:
            if self.price and self.pk:
                coin_management_instance = CoinManagement.objects.create(
                    coins=(int(self.price) * 15) / 100,
                    sale_coins=(int(self.price) * 25) / 100,
                    sale_price=(int(self.price) * 40) / 100,
                    deduction=(int(self.price) * 5) / 100,
                    product_id=self.pk)
                if coin_management_instance:
                    print('coins-automation-update')
                else:
                    print('coins-automation-failed')
        except Exception as e:
            print('G254')
            print(e)
            
        return super(Product, self).save(*args, **kwargs)



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    image = models.ImageField(upload_to=user_images, default='profile/default/default.png')
    total_price = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.user.username


class CartItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.product.name

    def add_amount(self):
        amount = self.product.price * self.quantity
        profile = self.user.profile
        profile.total_price = profile.total_price + amount
        profile.save()
        return True


@receiver(post_delete, sender=Profile)
def profile_image_delete(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(True)


@receiver(post_delete, sender=Product)
def product_image_delete(sender, instance, **kwargs):
    if instance.image:
        instance.image.delete(True)

class Orders(models.Model): #Model for all the orders placed
    cart = models.ForeignKey(CartItem, on_delete=models.CASCADE, null=True, blank=True)
    order_id = models.AutoField(primary_key=True)
    amount = models.IntegerField(default=0)
    First_name = models.CharField(max_length=90)
    last_name = models.CharField(max_length=90)
    email = models.CharField(max_length=70)
    address = models.CharField(max_length=500)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    zip_code = models.CharField(max_length=50)
    phone = models.CharField(max_length=50, default="")
    total_price = models.PositiveBigIntegerField(null=True, blank=True)
    payment_mode = models.CharField(max_length=150, null=False)
    payment_key = models.CharField(max_length=250, null=True)
    points = models.PositiveIntegerField(default=0, null=True, blank=True)
    orderstatuses = (
        ('Pending','Pending'),
        ('Out For Shipping','Out For Shipping'),
        ('Completed','Completed'),
        )
    status = models.CharField(max_length=150, choices=orderstatuses, default='Pending')
    message = models.CharField(max_length=150, null=True, blank=True)


class OrderUpdate(models.Model): #Model for all the updates for order tracking 
    update_id = models.AutoField(primary_key=True)
    order_id = models.ForeignKey(Orders, on_delete=models.CASCADE, null=True, blank=True)
    update_desc = models.CharField(max_length=5000)
    timestamp = models.TimeField(auto_now_add=True)

    def __str__(self):
        return self.update_desc[0:7] + "..."


class OrderItem(models.Model):
    order = models.ForeignKey(Orders, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.PositiveIntegerField(null=False)
    quantity = models.IntegerField(null=False)

    def _str_(self):
        return '{} {}'.format(self.order.id)
# Generated by Django 4.0.2 on 2022-02-27 23:40

import apis.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('configuration', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Brands',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=200)),
                ('is_lux', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'Brands',
                'verbose_name_plural': 'Brands',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=200)),
                ('is_lux', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Gender',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=200)),
                ('is_lux', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'Gender',
                'verbose_name_plural': 'Gender',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Orders',
            fields=[
                ('order_id', models.AutoField(primary_key=True, serialize=False)),
                ('amount', models.IntegerField(default=0)),
                ('First_name', models.CharField(max_length=90)),
                ('last_name', models.CharField(max_length=90)),
                ('email', models.CharField(max_length=70)),
                ('address', models.CharField(max_length=500)),
                ('city', models.CharField(max_length=50)),
                ('state', models.CharField(max_length=50)),
                ('zip_code', models.CharField(max_length=50)),
                ('phone', models.CharField(default='', max_length=50)),
                ('total_price', models.PositiveBigIntegerField(blank=True, null=True)),
                ('payment_mode', models.CharField(max_length=150)),
                ('payment_key', models.CharField(max_length=250, null=True)),
                ('points', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('status', models.CharField(choices=[('Pending', 'Pending'), ('Out For Shipping', 'Out For Shipping'), ('Completed', 'Completed')], default='Pending', max_length=150)),
                ('message', models.CharField(blank=True, max_length=150, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='OrderUpdate',
            fields=[
                ('update_id', models.AutoField(primary_key=True, serialize=False)),
                ('order_id', models.IntegerField(default='')),
                ('update_desc', models.CharField(max_length=5000)),
                ('timestamp', models.TimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sub_Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=200)),
                ('is_lux', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'Sub category',
                'verbose_name_plural': 'Sub_categories',
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(default='profile/default/default.png', upload_to=apis.models.user_images)),
                ('total_price', models.PositiveIntegerField(default=0)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=150, null=True, unique=True)),
                ('description', models.TextField()),
                ('slug', models.SlugField(blank=True, null=True, unique=True)),
                ('product_option', models.CharField(blank=True, max_length=200, null=True)),
                ('is_available', models.BooleanField(default=True)),
                ('quantity', models.PositiveIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('emoji', models.CharField(blank=True, max_length=200, null=True)),
                ('hot', models.CharField(blank=True, max_length=200, null=True)),
                ('status', models.CharField(blank=True, max_length=200, null=True)),
                ('like', models.BooleanField(default=False)),
                ('price', models.PositiveIntegerField(verbose_name='MRP')),
                ('discount', models.PositiveIntegerField(default=0)),
                ('discount_visible', models.CharField(blank=True, max_length=200, null=True)),
                ('old_price', models.PositiveIntegerField(blank=True, null=True)),
                ('old', models.CharField(blank=True, max_length=200, null=True)),
                ('image', models.ImageField(upload_to=apis.models.product_image, verbose_name='Image 1')),
                ('hover_image', models.ImageField(upload_to=apis.models.product_image, verbose_name='Image 2/ hover image')),
                ('image_3', models.ImageField(blank=True, null=True, upload_to=apis.models.product_image, verbose_name='Image 3')),
                ('image_4', models.ImageField(blank=True, null=True, upload_to=apis.models.product_image, verbose_name='Image 4')),
                ('image_5', models.ImageField(blank=True, null=True, upload_to=apis.models.product_image, verbose_name='Image 5')),
                ('image_6', models.ImageField(blank=True, null=True, upload_to=apis.models.product_image, verbose_name='Image 6')),
                ('Brands', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='apis.brands')),
                ('Sub_Category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='apis.sub_category')),
                ('bust', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bust_prduct', to='configuration.bustmaster')),
                ('category', models.ManyToManyField(related_name='products', to='apis.Category')),
                ('cloth_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cloth_type_prduct', to='configuration.clothtypesmaster')),
                ('code_name', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product_prduct', to='configuration.productcodemaster')),
                ('code_size', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cloth_code_prduct', to='configuration.clothcodesmaster', verbose_name='Size Code')),
                ('colour', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cloth_color_prduct', to='configuration.clothcolourmaster')),
                ('condition', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='condtion_prduct', to='configuration.productconditionmaster')),
                ('hip', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hip_prduct', to='configuration.hipmaster')),
                ('length', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='lenth_prduct', to='configuration.lengthmaster')),
                ('material_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='material_prduct', to='configuration.materialtypemaster')),
                ('size', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='size_prduct', to='configuration.sizemaster')),
                ('user', models.ForeignKey(on_delete=models.SET(apis.models.get_superuser), to=settings.AUTH_USER_MODEL)),
                ('waist', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='waiset_prduct', to='configuration.waistmaster')),
            ],
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price', models.PositiveIntegerField()),
                ('quantity', models.IntegerField()),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='apis.orders')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='apis.product')),
            ],
        ),
        migrations.CreateModel(
            name='CartItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(default=1)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='apis.product')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

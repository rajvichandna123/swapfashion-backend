from warnings import filters
from django.contrib import admin
from jmespath import search
from .models import  Gender, Product, Category, CartItem, Profile, Brands, Sub_Category, Orders, OrderUpdate



# @admin.register(Product)
# class ProductAdmin(admin.ModelAdmin):
#     """ adding the product class to the admin site """
#     exclude = ['old_price']
#     list_display = (
#         'name', "price", "quantity", "is_available", 'created', "discount")

#     search_fields = ("name", "category",)
#     date_hierarchy = "created"
#     list_editable = ['price', 'is_available', 'quantity', "discount"]
#     prepopulated_fields = {'slug': ("name",)}


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """ adding category class to the admin site """

    list_display = ("name", "slug", "is_lux")
    list_editable = ['is_lux']
    prepopulated_fields = {'slug': ("name",)}

@admin.register(Sub_Category)
class Sub_CategoryAdmin(admin.ModelAdmin):
    """ adding Sub_category class to the admin site """

    list_display = ("name", "slug", "is_lux")
    list_editable = ['is_lux']
    prepopulated_fields = {'slug': ("name",)}

@admin.register(Brands)
class BrandsAdmin(admin.ModelAdmin):
    """ adding Brands class to the admin site """

    list_display = ("name", "slug", "is_lux")
    list_editable = ['is_lux']
    prepopulated_fields = {'slug': ("name",)}

@admin.register(Gender)
class GenderAdmin(admin.ModelAdmin):
    """ adding Gender class to the admin site """

    list_display = ("name", "slug", "is_lux")
    list_editable = ['is_lux']
    prepopulated_fields = {'slug': ("name",)}

class ProductAdmin(admin.ModelAdmin):
    """ProductAdmin
    """
    search_fields = [
        'sku',
        'title',
    ]

    list_filter = [
        'created',
        'Brands',
        'cloth_type',
        'code_size',
        'colour',
        'condition',
        'material_type',
        "bust",
        'waist',
        'hip',
        'length',
        'size',
        'code_name',
    ]


    exclude = [
        'sku',
        'title',
        'created',
    ]
    list_display = [
        'sku',
        'title',
        # 'name',
        # 'slug',
        # # 'category',
        # 'Sub_Category',
        # 'product_option',
        # 'price',
        # 'discount',
        # 'discount_visible',
        # 'is_available',
        # 'quantity',
        # 'available',
        
        # 'image',
        # 'hover_image',
        # 'old_price',
        # 'old',
        # 'description',
        # 'emoji',
        # 'hot',
        # 'status',
        # 'like',
        'Brands',
        'cloth_type',
        'code_size',
        'colour',
        'condition',
        'material_type',
        "bust",
        'waist',
        'hip',
        'length',
        'size',
        'code_name',
        'price',
        'user',
        'created',

    ]

    fields = [
        # 'sku',
        # 'title',
        'Brands',
        'description',
        'image',
        'hover_image',
        'image_3',
        'image_4',
        'image_5',
        'image_6',
        'quantity',
        'is_available',
        'like',
        
        'cloth_type',
        'code_size',
        'colour',
        'condition',
        'material_type',
        "bust",
        'waist',
        'hip',
        'length',
        'size',
        'code_name',
        'price',
        'user',
        'created',
    ]

    readonly_fields = [
        # 'sku',
        # 'title',
        
        'created',
    ]

admin.site.register(Product, ProductAdmin)
admin.site.register(Orders)
admin.site.register(OrderUpdate)
admin.site.register(CartItem)
admin.site.register(Profile)

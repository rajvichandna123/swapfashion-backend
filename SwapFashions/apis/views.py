from lib2to3.pgen2.tokenize import generate_tokens
from tracemalloc import Filter
from django.http import HttpResponse
from rest_framework import viewsets, permissions, generics, status
from .models import (
    Brands, 
    Gender,
    OrderItem, 
    Product, 
    Category, 
    CartItem,
    Sub_Category,
    Orders,
    OrderUpdate
) 

from .serializers import (
    BrandsSerializer, 
    GenderSerializer, 
    ProductSerializer, 
    CategorySerializer, 
    CartItemSerializer, 
    Sub_CategorySerializer, 
    UserSerializer,
    CartItemAddSerializer,
    OrderSerializer
)
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework import viewsets
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import filters
import json



class CategoryView(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    '''permission_classes = [
        permissions.IsAdminUser, permissions.IsAuthenticatedOrReadOnly]'''
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']


class Sub_CategoryView(viewsets.ModelViewSet):
    queryset = Sub_Category.objects.all()
    serializer_class = Sub_CategorySerializer

    filter_backends = [filters.SearchFilter]
    search_fields = ['name']


class BrandsView(viewsets.ModelViewSet):
    queryset = Brands.objects.all()
    serializer_class = BrandsSerializer
    
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']


class GenderView(viewsets.ModelViewSet):
    queryset = Gender.objects.all()
    serializer_class = GenderSerializer
    
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']

class OrderModelViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = (permissions.IsAuthenticated, )
    filter_backends = [filters.SearchFilter]
    search_fields = [
        'product__name', 'product__description', 'product__category__name']

    def get_queryset(self):
        user = self.request.user
        return OrderItem.objects.filter(user=user)


class ProductView(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    '''permission_classes = [
        permissions.IsAdminUser, permissions.IsAuthenticatedOrReadOnly]'''
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'description', 'category__name']


class UserView(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class CartItemView(generics.ListAPIView):
    serializer_class = CartItemSerializer
    permission_classes = (permissions.IsAuthenticated, )
    filter_backends = [filters.SearchFilter]
    search_fields = [
        'product__name', 'product__description', 'product__category__name']

    def get_queryset(self):
        user = self.request.user
        return CartItem.objects.filter(user=user)


class CartItemAddView(generics.CreateAPIView):
    queryset = CartItem.objects.all()
    serializer_class = CartItemAddSerializer
    permission_classes = (permissions.IsAuthenticated, )


class CartItemDelView(generics.DestroyAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    queryset = CartItem.objects.all()

    def delete(self, request, pk, format=None):
        user = request.user
        cart_item = CartItem.objects.filter(user=user)
        target_product = get_object_or_404(cart_item, pk=pk)
        product = get_object_or_404(Product, id=target_product.product.id)
        product.quantity = product.quantity + target_product.quantity
        product.save()
        target_product.delete()
        return Response(status=status.HTTP_200_OK, data={"detail": "deleted"})


class CartItemAddOneView(APIView):
    permission_classes = (permissions.IsAuthenticated, )

    def get(self, request, pk, format=None):
        user = request.user
        cart_item = CartItem.objects.filter(user=user)
        target_product = cart_item.get(pk=pk)
        product = get_object_or_404(Product, id=target_product.product.id)
        if product.quantity <= 0:
            return Response(
                data={
                    "detail": "this item is sold out try another one !",
                    "code": "sold_out"})

        target_product.quantity = target_product.quantity + 1
        product.quantity = product.quantity - 1
        product.save()
        target_product.save()
        return Response(
            status=status.HTTP_226_IM_USED,
            data={"detail": 'one object added', "code": "done"})


class CartItemReduceOneView(APIView):
    permission_classes = (permissions.IsAuthenticated, )

    def get(self, request, pk, format=None):
        user = request.user
        cart_item = CartItem.objects.filter(user=user)
        target_product = cart_item.get(pk=pk)
        product = get_object_or_404(Product, id=target_product.product.id)
        if target_product.quantity == 0:
            return Response(
                data={
                    "detail": "there is no more item like this in tour cart",
                    "code": "no_more"})

        target_product.quantity = target_product.quantity - 1
        product.quantity = product.quantity + 1
        product.save()
        target_product.save()
        return Response(
            status=status.HTTP_226_IM_USED,
            data={
                "detail": 'one object deleted',
                "code": "done"
            })

class Checkout(APIView):
    serializer_class = OrderSerializer

    permission_classes = (permissions.IsAuthenticated, )

    def post(self, request): #To place the order and Checkout
        
        try:
            Order_serializer = self.serializer_class(data=request.data)
            if Order_serializer.is_valid(raise_exception=True):
                Order_serializer.save()
                try:
                    update = OrderUpdate(
                        order_id=Order_serializer['order_id'],
                        update_desc="The order has been placed")
                    update.save()
                    thank = True
                    id = Order_serializer['order_id']
                except Exception as e:
                    print('G197')
                    print(e)
                return Response(
                    status=status.HTTP_200_OK,
                    data={"detail": 'Thank you for shoping with us', "code": "done"})
        except Exception as e:
            return Response({"message": str(e)})



def tracker(request): #To track an existing order placed by the user
    if request.method=="POST":
        orderId = request.POST.get('orderId', '')
        email = request.POST.get('email', '')
        try:
            order = Orders.objects.filter(order_id=orderId, email=email)
            if len(order)>0:
                update = OrderUpdate.objects.filter(order_id=orderId)
                updates = []
                for item in update:
                    updates.append({'text': item.update_desc, 'time': item.timestamp})
                return HttpResponse(json.dumps({
                    "status":"success",
                    "updates": updates, 
                    "itemsJson": order[0].items_json}, default=str))
            else:
                return HttpResponse('{"status":"noitem"}')
        except Exception as e:
            return HttpResponse('{"status":"error"}')

    return Response(
        status=status.HTTP_200_OK,
            data={
                "detail": 'detail about product location',
                "code": "finding"
            })
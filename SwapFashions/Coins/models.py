from itertools import product
from pyexpat import model
from re import T
from statistics import mode
from django.db import models

# Create your models here.
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User



# Create your models here.


class Coin(models.Model):
	# admin creator
    creator = models.OneToOneField(
        settings.AUTH_USER_MODEL, related_name='coins', 
        on_delete=models.CASCADE)
    name = models.CharField(max_length=700, null=True, blank=True)
    number_available = models.IntegerField(default=0)
    coins = models.IntegerField(default=0, null=True, blank=True)
    end_bid_time = models.DateTimeField()
    products = models.ForeignKey('apis.Product', on_delete=models.CASCADE, null=True, blank=True)


class CoinManagement(models.Model):
    """CoinManagement
    """
    product = models.ForeignKey('apis.Product', on_delete=models.CASCADE)
    coins = models.PositiveIntegerField(verbose_name='User Coins Value', help_text="15% of MRP")
    sale_coins = models.PositiveIntegerField(help_text="25% of MRP")
    sale_price = models.PositiveIntegerField(verbose_name='Sale Price Value', help_text="40% of MRP")
    deduction = models.PositiveIntegerField(help_text="5% of MRP")
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, 
        null=True, blank=True,
        help_text='Assign Coins to user')

    class Meta:
        verbose_name = " Product Price Management"
        verbose_name_plural = " Product Price Management"
        db_table = 'product_price_management'
    
    def __str__(self):
        try:
            return str(self.product.Brands.name)
        except Exception as e:
            print(e)
            return '-'

    def get_product_title(self):
        """
        get_product_title
        """
        try:
            return str(self.product.title)
        except Exception as e:
            print('G53')
            print('get_product_title.exception.message')
            print(e)

    get_product_title.short_description = 'Product Title.'

    def get_product_mrp_price(self):
        """
        get_product_mrp_price
        """
        try:
            return str(self.product.price) + '₹'
        except Exception as e:
            print('63')
            print('get_product_mrp_price.exception.message')
            print(e)

    get_product_mrp_price.short_description = 'MRP.'

    def save(self, *args, **kwargs):
        """
        save function used to automation the coin user records tables
        """
        try:
            if self.user and self.coins:
                user_coin_instance =  UserCoinsRecords.objects.filter(
                    user=self.user).last()
                if user_coin_instance:
                    UserCoinsRecords.objects.filter(
                        user=self.user
                    ).update(coins=int(user_coin_instance.coins) + int(self.coins))
                    print('coins-user-update')
                else:
                    create_coin_instance = UserCoinsRecords.objects.create(
                        user=self.user,
                        coins=int(self.coins))
                    if create_coin_instance:
                        print('coins-user-update')
                    else:
                        print('coins-user-failed')
            else:
                print('user-coin-free-pass')
        except Exception as e:
            print('G258')
            print(e)
            
        return super(CoinManagement, self).save(*args, **kwargs)


class CoinsDeductionSystem(models.Model):
    """
    CoinsDeductionSystem
    """
    product = models.ForeignKey('apis.Product', on_delete=models.CASCADE)
    percentage = models.PositiveIntegerField(help_text="In %") 
    deduction = models.PositiveIntegerField(help_text="percentage% of MRP", null=True, blank=True)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, 
        null=True, blank=True,
        help_text='Assign Coins to user')

    class Meta:
        verbose_name = "Coins Deduction Management"
        verbose_name_plural = "Coins Deduction Management"
        db_table = 'coin_deduction_system'
    
    def __str__(self):
        try:
            return str(self.user.username)
        except Exception as e:
            print(e)
            return '-'

    def get_product_title(self):
        """
        get_product_title
        """
        try:
            return str(self.product.title)
        except Exception as e:
            print('G53')
            print('get_product_title.exception.message')
            print(e)

    get_product_title.short_description = 'Product.'

    def get_product_mrp_price(self):
        """
        get_product_mrp_price
        """
        try:
            return str(self.product.price) + '₹'
        except Exception as e:
            print('63')
            print('get_product_mrp_price.exception.message')
            print(e)

    get_product_mrp_price.short_description = 'MRP.'


    def save(self, *args, **kwargs):
        """
        save function used to automation the coin user records tables
        """
        try:
            deduction_value = int((int(self.product.price) \
                * int(self.percentage)) / 100)
            if self.user and self.percentage:
                user_coin_instance =  UserCoinsRecords.objects.filter(user=self.user).last()
                if user_coin_instance:
                    UserCoinsRecords.objects.filter(
                        user=self.user
                    ).update(coins=int(user_coin_instance.coins) 
                        - int(deduction_value))
                    self.deduction = int(deduction_value)
                    print('coins-user-update')
                else:
                    print('deduction-not-valid')
            elif self.percentage:
                self.deduction = int(deduction_value)
                
        except Exception as e:
            print('G175')
            print(e)
            
        return super(CoinsDeductionSystem, self).save(*args, **kwargs)


class UserCoinsRecords(models.Model):
    """UserCoinsRecords
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_coins')
    coins = models.PositiveIntegerField(default=0, null=True, blank=True)

    def __str__(self):
        try:
            return str(self.user.username)
        except Exception as e:
            print(e)
            return '-'


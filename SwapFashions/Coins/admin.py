from dataclasses import field
from django.contrib import admin
from Coins.models import *

from Coins.models import CoinManagement, UserCoinsRecords, CoinsDeductionSystem


class CoinManagementAdmin(admin.ModelAdmin):
    """
    CoinManagementAdmin : search, filter and display in admin panel..
    """
    # exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'is_active']
    search_fields = ['product__sku', 'product__title']
    # list_filter = ['is_active', ('created_at', DateRangeFilter), ('updated_at', DateRangeFilter), 'created_by', 'updated_by']
    list_filter = ['product']
    list_display = [
        'get_product_title',
        'get_product_mrp_price',
        'coins',
        'sale_coins',
        'sale_price',
        # 'deduction',
        'user',
    ]
    fields = [
        'get_product_title',
        'get_product_mrp_price',
        'coins',
        'user',
        'sale_coins',
        'sale_price',
        # 'deduction',
    ]
    readonly_fields = [
        'get_product_title',
        'get_product_mrp_price',
        'coins',
        'sale_coins',
        'sale_price',
        # 'deduction',
    ]

    # actions = [make_active_data, make_deactive_data]
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        """
        has_delete_permission: used to remove delete functionalty in admin panel.
        """
        return False

    def has_add_permission(self, request, obj=None):
        """
        has_add_permission: used to add and not add permission in adin panel.
        """
        return True

    # def save_model(self, request, obj, form, change):
    #     """
    #     save_model: used to change and update activity from admin panel.
    #     """
    #     pass
    #     # if not change:
    #     #     obj.created_by = request.user
    #     # else:
    #     #     obj.updated_by = request.user
    #     #     obj.updated_at = datetime.now()
    #     # obj.save()



class UserCoinsRecordsAdmin(admin.ModelAdmin):
    """
    UserCoinsRecordsAdmin : search, filter and display in admin panel..
    """
    # exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'is_active']
    search_fields = ['user__username', 'coins']
    # list_filter = ['is_active', ('created_at', DateRangeFilter), ('updated_at', DateRangeFilter), 'created_by', 'updated_by']
    list_filter = ['user']
    list_display = [
        'user',
        'coins',
    ]
    fields = [
        'user',
        'coins',
    ]
    readonly_fields = [
        'user',
        'coins',
    ]

    # actions = [make_active_data, make_deactive_data]
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        """
        has_delete_permission: used to remove delete functionalty in admin panel.
        """
        return False

    def has_add_permission(self, request, obj=None):
        """
        has_add_permission: used to add and not add permission in adin panel.
        """
        return True

    # def save_model(self, request, obj, form, change):
    #     """
    #     save_model: used to change and update activity from admin panel.
    #     """
    #     pass
    #     # if not change:
    #     #     obj.created_by = request.user
    #     # else:
    #     #     obj.updated_by = request.user
    #     #     obj.updated_at = datetime.now()
    #     # obj.save()





class CoinsDeductionSystemAdmin(admin.ModelAdmin):
    """
    UserCoinsRecordsAdmin : search, filter and display in admin panel..
    """
    # exclude = ['created_at', 'updated_at', 'created_by', 'updated_by', 'is_active']
    search_fields = ['product__sku', 'product__title']
    # list_filter = ['is_active', ('created_at', DateRangeFilter), ('updated_at', DateRangeFilter), 'created_by', 'updated_by']
    list_filter = ['product']
    list_display = [
        'get_product_title',
        'get_product_mrp_price',
        'percentage',
        'deduction',
        'user',
    ]
    fields = [
        # 'get_product_title',
        # 'get_product_mrp_price',
        'product',
        'percentage',
        'deduction',
        'user',
    ]
    readonly_fields = [
        'get_product_title',
        'get_product_mrp_price',
        'deduction',
        # 'coins',
        # 'sale_coins',
        # 'sale_price',
        # 'deduction',
    ]


    # actions = [make_active_data, make_deactive_data]
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        """
        has_delete_permission: used to remove delete functionalty in admin panel.
        """
        return False

    def has_add_permission(self, request, obj=None):
        """
        has_add_permission: used to add and not add permission in adin panel.
        """
        return True

    # def save_model(self, request, obj, form, change):
    #     """
    #     save_model: used to change and update activity from admin panel.
    #     """
    #     pass
    #     # if not change:
    #     #     obj.created_by = request.user
    #     # else:
    #     #     obj.updated_by = request.user
    #     #     obj.updated_at = datetime.now()
    #     # obj.save()


admin.site.register(CoinManagement, CoinManagementAdmin)
admin.site.register(CoinsDeductionSystem, CoinsDeductionSystemAdmin)
admin.site.register(UserCoinsRecords, UserCoinsRecordsAdmin)


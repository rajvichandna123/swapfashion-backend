from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

# Create your models here.


class Customer(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='customer',
                                on_delete=models.CASCADE)
    # created customer models cus its extendable and also seperate from admin
    



class TwoFactorAuthentication(models.Model):
    """
    TwoFactorAuthentication
    mobile and email 
    authentication module
    """
    twoFA_type = models.CharField(
        max_length=20,
        choices=(
            ('link', 'link'),
            ('otp', 'OTP'),
        )
    )
    activation_link = models.TextField(null=True, blank=True)
    otp = models.PositiveIntegerField(null=True)
    delivery_status = models.CharField(
        max_length=20,
        choices=(
            ('delivered', 'Delivered'),
            ('not_delivered', 'Not Delivered'),
            ('successful', 'Successful'),
            ('expired', 'Expired')
        )
    )
    user_type = models.CharField(
        max_length=20,
        choices=(
            ('customer', 'Customer'), ('admin', 'Admin'),
        )
    )
    auth_type = models.CharField(
        max_length=20,
        choices=(
            ('phone', 'Phone Number'), ('email', 'Email ID')
        )
    )

    # verification flags

    created_at = models.DateTimeField(auto_now_add=True)
    expired_datetime = models.DateTimeField(verbose_name="Expired At")
    is_verified = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = '  Two Factor Authentication'
        verbose_name_plural = '  Two Factor Authentication'
        db_table = 'two_factor_authentication'
        ordering = ['-created_at']

    def __str__(self):
        return str(self.otp)

    def get_user_name(self):
        """
        created at 2021-08-31 by Gourav Sharma (^_^) get_user_name
        This "get_user_name" method used to get the username to get the username
        return: (username)
        """
        if self.user:
            return str(self.user.username) if self.user.username else '-'
        else:
            return '-'

    get_user_name.short_description = 'Email'
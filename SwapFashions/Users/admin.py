from django.contrib import admin
from Users.models import *

admin.site.register(Customer)






class TwoFactorAuthenticationAdmin(admin.ModelAdmin):
    """
    TwoFactorAuthenticationAdmin : search, filter and display in admin panel..
    """

    ordering = ('-created_at',)

    search_fields = [
        # 'user__email',
        'user__username'
    ]
    list_filter = [
        'delivery_status',
        'auth_type',
        'created_at',
        'expired_datetime',
    ]
    list_display = [
        'get_user_name',
        'otp',
        'delivery_status',
        'auth_type',
        'created_at',
        'expired_datetime'
    ]
    readonly_fields = [
        'get_user_name',
        'otp',
        'delivery_status',
        'auth_type',
        'created_at',
        'expired_datetime'
    ]

    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        """
        has_delete_permission: used to remove delete functionalty in admin panel.
        """
        return False

    def has_add_permission(self, request, obj=None):
        """
        has_add_permission: used to add and not add permission in adin panel.
        """
        return False


# Registered TwoFactorAuthenticationAdmin table ..


admin.site.register(TwoFactorAuthentication, TwoFactorAuthenticationAdmin)


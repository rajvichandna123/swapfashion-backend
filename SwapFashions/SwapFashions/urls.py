"""SwapFashions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from django.views.generic import TemplateView


# from rest_framework_swagger.views import get_swagger_view
# from rest_framework.schemas import get_schema_view

# schema_view = get_swagger_view(title='Swap Fashin API')


urlpatterns = [
    # path('docs/', include('rest_framework_docs.urls')),
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path('admin/', admin.site.urls),
    path('apis/', include('apis.urls')),
    path('', include('swap_fashions.urls')),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('auth/', include('auth.urls')),
    path("contact/", include('contactus.urls')),
    path('razorpay/', include('razorpay.urls')),
    path('user/', include('Users.urls', namespace='Users')),
    path('bid/', include('Bid.urls', namespace='Bid')),
    path('api/v1/', include('swap_fashion_apis.urls')),

    # # swagger api docs
    # path('docs', schema_view),
    # path('openapi', get_schema_view(
    #     title="Swap Fashion",
    #     description="API for all things …", 
    #     version="1.0.0"), name='openapi-schema'),
    # path('api-docs/', TemplateView.as_view(template_name='swagger-ui.html',
    #                                        extra_context={'schema_url':'openapi-schema'}
    #                                        ), name='swagger-ui'),
    # path('redoc/', TemplateView.as_view(
    #     template_name='redoc.html',
    #     extra_context={'schema_url':'openapi-schema'}), name='redoc'),



]


"""
1) title not coming correct
SF(swap fashions)22(year)02(month)29(code)C(size code) (serial Number )
"""


# admin conf
admin.site.site_header = 'Swap Fashions'
admin.site.index_title = 'Swap Fashions'
admin.site.site_title = 'Swap Fashions'

from django.shortcuts import render


def index(request):
    return render(request, 'swap_fashions/index.html')
from django.apps import AppConfig


class SwapFashionsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'swap_fashions'
